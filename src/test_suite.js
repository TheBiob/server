
import { close_and_exit } from "./index.js";

import fs from "fs";
import { SmartBuffer } from "smart-buffer";

const serverPackages = {};
// TODO: implement verbose logging if config says so
// TODO: write out received packages that fail if keep is set

// TODO: refactor communication between test suite and server
//   log messages directly to test suite

/**
 * Loads all test packages from the server_packages/ directory (*.tcp/*.udp) and starts listening for messages from the parent process.
*/
export function initialize() {
    process.on('message', parentMessageListener);
}

let next_packet = [];
let current_package_num = 0;
/**
 * Checks if message equals the next expected message, if there are any
 * @param {SmartBuffer} message 
 */
function check_expected_packets(message) {
    if (next_packet.length !== 0) {
        current_package_num++;
        const packet = next_packet.shift();
        // if (config.verbose)
        //     console.log(packet);

        if (!message.toBuffer().equals(packet.buffer)) {
            let msg = `Expected package ${current_package_num} (${packet.name}) differed from received package`;
            if (config !== undefined && config.verbose) {
                msg += '. Received: ';
                msg += Array.from(message.toBuffer()).map(x => x.toString(16).padStart(2, '0')).join(' ');
                msg += ' != Expected: ';
                msg += Array.from(packet.buffer).map(x => x.toString(16).padStart(2, '0')).join(' ');
            }
            fail(msg);
        }
    }
}

function getTypeName(num) {
    let msg = num.toString();
    switch (num) {
        case 0: msg += ' (CREATED)'; break;
        case 1: msg += ' (DESTROYED)'; break;
        case 2: msg += ' (HEARTBEAT)'; break;
        case 3: msg += ' (NAME)'; break;
        case 4: msg += ' (CHAT MESSAGE)'; break;
        case 5: msg += ' (SAVE)'; break;
        case 100: msg += ' (TEST_REPLY)'; break;
        case 101: msg += ' (TEST_EXPECT)'; break;
        case 200: msg += ' (TEST_OK)'; break;
        case 201: msg += ' (TEST_FAIL)'; break;
        default: msg += ' (UNKNOWN)'; break;
    }
    return msg;
}

/**
 * Intercepts messages and checks if the server should respond with test packages
 * @param {{onMessage, sendMessage}} player 
 * @param {SmartBuffer} message 
*/
export function testSuiteOnSocketMessage(player, message) {
    // Config must be set up
    if (config === undefined) {
        config = {error:true};
        fail(`Config was not set`);
        return;
    } else if (config.error !== undefined) {
        return;
    }

    const type = message.readUInt8();
    if (config.verbose)
        console.log('Message: ' + getTypeName(type));
    if (type < 100) {
        message.readOffset -= 1; // Reset message offset and pass to default message handler

        check_expected_packets(message);

        player.onMessage(message);
    } else switch (type) {
        case 100: // Reply with package
            const packet = getPacket(message);
            if (packet !== undefined) {
                const buffer = new SmartBuffer();
                buffer.writeBuffer(packet.buffer);
                player.sendMessage(buffer);
            }
            break;

        case 101: // Expect package
            const expectedPacket = getPacket(message);
            if (expectedPacket !== undefined) {
                next_packet.push(expectedPacket);
            }
            break;

        case 200: // 200 - OK, stop the test suite, everything on client side was ok
            pass();
            break;
        
        case 201: // 201 - FAIL, something failed. It'll usually be written to game_errors.log but some GMS sandboxes may not allow writing to the program directory
            fail(message.readStringNT());
            break;

        default:
            //fail(`Unknown message ${type}`);
            break;
    }
}

function getPacket(message) {
    const packetName = message.readStringNT();
    const packet = serverPackages[packetName];
    if (packet === undefined) {
        fail(`Server package '${packetName}' was not found.`);
        return undefined;
    }

    if (packet.type !== 'tcp') {
        // TODO: figure out how to handle UDP packages
        fail(`Server package '${packetName}' is of type '${packet.type}' which is not currently supported`);
        return undefined;
    }

    return packet;
}

const log = [];
let failed = false;
let passed = false;
function fail(msg) {
    log.push(msg);
    failed = true;
}
function pass() {
    passed = true;
}

function parentMessageListener(message) {
    console.log(JSON.stringify(message));
    if (typeof message === 'object') {
        if (message.name === 'get_result_and_close') {
            if (next_packet.length != 0) {
                fail(`${next_packet.length} expected packet(s) left in queue when result was requested.`);
            }
            process.send({
                name: 'get_result_and_close',
                failed,
                passed,
                log,
            });
            close();
        } else if (message.name === 'config') {
            config = message.config;
            for (const name in message.server_packages) {
                const packet = message.server_packages[name];
                serverPackages[name] = {
                    name: name,
                    type: packet.type,
                    buffer: Buffer.from(packet.data, 'base64'),
                };
            }
        } else {
            fail(`Unknown message '${message.name}'`);
            close();
        }
    } else {
        fail(`Unknown message type '${typeof message}'`);
        close();
    }
}

function close() {
    if (log.length !== 0) {
        fs.writeFileSync('./server.log', log.join('\n'));
        close_and_exit(1);
    } else {
        close_and_exit(0);
    }
}
